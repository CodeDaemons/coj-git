# 1394 - An Inductively-Defined Function

## Description

Consider the function f which is inductively defined on the positive integers,
as follows:
f(1) = 1
f(2n) = n
f(2n+1) = f(n) + f(n+1)
Given a positive integer value for n (greater than or equal to 1),
find the value of f(n).

## Input specification

The input consists of a sequence of positive integer values for n
followed by -1.
The integers can be preceded and/or followed by whitespace.

## Output specification

For each positive integer n,
display the value of n and the value of f(n).
Use the format shown in the example below,
and leave a blank line between the output for each value of n.

## Sample input

```
2
53
153
-1
```

## Sample output

```
f(2) = 1

f(53) = 27

f(153) = 77
```

## Hint(s)

## Recommendation

We have carefully selected several similar problems:
[3376](http://coj.uci.cu/24h/problem.xhtml?pid=3376) |
[2441](http://coj.uci.cu/24h/problem.xhtml?pid=2441) |
[2534](http://coj.uci.cu/24h/problem.xhtml?pid=2534) |
[2141](http://coj.uci.cu/24h/problem.xhtml?pid=2151) |
[3232](http://coj.uci.cu/24h/problem.xhtml?pid=3232) |
[1070](http://coj.uci.cu/24h/problem.xhtml?pid=1070)

## Source

[An Inductively-Defined Function](http://coj.uci.cu/24h/problem.xhtml?pid=1394)
