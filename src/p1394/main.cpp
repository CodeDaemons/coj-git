/* Copyright (c) 2018 Kevin Villalobos */
#include <iostream>
#include "p1394.h"

namespace tools = an_inductively_defined_function;

int main() {
    int64 n;

    while (scanf("%lld", &n)) {
        if (n == -1) break;

        std::cout << "f(" << n << ") = " << tools::f_n(n) << std::endl;
    }

    return 0;
}
