# 3327 -How Many Numbers 

## Description

How many integer numbers (including negative integers) can be written,
in its decimal representation,
with exactly E even digits and O odd digits?

## Input specification

Input consists of several test cases,
no more than 200.
Each case consists of a line with a pair of integers E and O
separated by exactly one blank space.
These values represent the number of even and odd decimal digits,
respectively, that numbers you must write should have.
E and O satisfy the conditions E >= 0, O >= 0, and 0 < E + O <= 18.
Last line of input is followed by a line containing two zeros,
which should not be processed.

## Output specification

For each test case,
output a line with an integer representing the count of integers
that meet the above conditions.

## Sample input

```
1 1
2 0
0 2
0 0
```

## Sample output

```
90
40
50
```

## Hint(s)

## Recommendation

We have carefully selected several similar problems:
[3378](http://coj.uci.cu/24h/problem.xhtml?pid=3378) |
[2156](http://coj.uci.cu/24h/problem.xhtml?pid=2156) |
[3599](http://coj.uci.cu/24h/problem.xhtml?pid=3599) |
[3147](http://coj.uci.cu/24h/problem.xhtml?pid=3147) |
[3479](http://coj.uci.cu/24h/problem.xhtml?pid=3479) |
[1103](http://coj.uci.cu/24h/problem.xhtml?pid=1103)

## Source

[How Many Numbers](http://coj.uci.cu/24h/problem.xhtml?pid=3327)
