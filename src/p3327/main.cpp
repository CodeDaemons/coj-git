/* Copyright (c) 2018 Kevin Villalobos */
#include <iostream>
#include "p3327.h"

namespace tools = how_many_numbers;

int main() {
    int a, b;

    while (std::cin >> a >> b
        && (a || b)) {
        std::cout << tools::eo_numbers(a, b) << std::endl;
    }

    return 0;
}
