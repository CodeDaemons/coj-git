/* Copyright (c) 2018 Kevin Villalobos */
#include <iostream>
#include "p3392.h"

namespace tools = factory_of_painted_wodden_toys;

int main() {
    int N;

    while (std::cin >> N && N) {
        std::cout << tools::cubes(N) << std::endl;
    }

    return 0;
}
