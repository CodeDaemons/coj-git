/* Copyright (c) 2018 Kevin Villalobos */
#ifndef SRC_P3392_P3392_H_
#define SRC_P3392_P3392_H_

namespace factory_of_painted_wooden_toys {

int cubes(const int &N);

}

#endif  // SRC_P3392_P3392_H_
