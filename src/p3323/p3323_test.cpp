/* Copyright (c) 2018 Kevin Villalobos */
#include "p3323.h"

#include <utility>

#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

namespace utf = boost::unit_test;
namespace tools = even_number_of_divisors;

BOOST_AUTO_TEST_CASE(coj_test_1,
    *utf::timeout(1)) {
    const std::pair<int, int> query(1, 2);
    const int result = tools::enod_numbers(query);

    BOOST_REQUIRE_EQUAL(1, result);
}

BOOST_AUTO_TEST_CASE(coj_test_2,
    *utf::timeout(1)) {
    const std::pair<int, int> query(1, 3);
    const int result = tools::enod_numbers(query);

    BOOST_REQUIRE_EQUAL(2, result);
}

BOOST_AUTO_TEST_CASE(coj_test_3,
    *utf::timeout(1)) {
    const std::pair<int, int> query(2, 5);
    const int result = tools::enod_numbers(query);

    BOOST_REQUIRE_EQUAL(3, result);
}

BOOST_AUTO_TEST_CASE(test_square_extremes,
    *utf::timeout(1)) {
    const std::pair<int, int> query(4, 9);
    const int result = tools::enod_numbers(query);

    BOOST_REQUIRE_EQUAL(4, result);
}

BOOST_AUTO_TEST_CASE(test_square_extreme,
    *utf::timeout(1)) {
    const std::pair<int, int> query(3, 9);
    const int result = tools::enod_numbers(query);

    BOOST_REQUIRE_EQUAL(5, result);
}


BOOST_AUTO_TEST_CASE(test_long,
    *utf::timeout(1)) {
    const std::pair<int, int> query(1, 1e5);
    const int result = tools::enod_numbers(query);

    BOOST_REQUIRE_EQUAL(99684, result);
}

BOOST_AUTO_TEST_CASE(test_many_long_tests,
    *utf::timeout(1)) {
    int max_tests = 1000;

    while (max_tests--) {
        const std::pair<int, int> query(1, 1e5);
        const int result = tools::enod_numbers(query);

        BOOST_REQUIRE_EQUAL(99684, result);
    }
}
