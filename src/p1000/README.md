# 1000 - A+B Problem

## Description

For this problem you must calculate **A + B**, numbers given in the input.

## Input specification

The only line of input contain two space separated integers
**A, B (0 <= A, B <= 10)**.

## Output specification

The only line of output should contain one integer: the sum of **A** and **B**.

## Sample input

```
1 2
```

## Sample output

```
3
```

## Hint(s)

Read our **FAQs** carefully to see solution samples. 

## Recommendation

We have carefully selected several similar problems:
[1049](http://coj.uci.cu/24h/problem.xhtml?pid=1049) |
[1494](http://coj.uci.cu/24h/problem.xhtml?pid=1494) |
[1028](http://coj.uci.cu/24h/problem.xhtml?pid=1028) |
[3599](http://coj.uci.cu/24h/problem.xhtml?pid=3599) |
[1960](http://coj.uci.cu/24h/problem.xhtml?pid=1960) |
[3147](http://coj.uci.cu/24h/problem.xhtml?pid=3147)

## Source

[A+B Problem](http://coj.uci.cu/24h/problem.xhtml?pid=1000)
