# 3147 - Sum Challenge

## Description

Given a set of signed integers,
find if there is a non-empty consecutive subset whose sum is zero.

Examples:
L = [-4, -7, 10, 6, 4] -: NO
L = [-5, -2, -3, 5, 8] -: YES (because [-2, -3, 5] sums to zero).

## Input specification

The first line contain a integer number 1 <= T <= 100
representing the number of test cases.
For each test case will be a line with a integer number
1 <= N <= 50, followed by N integers between -100 and 100
(all numbers in the line are space-separated).

## Output specification

For each test case you must print a line with the word
YES if a non-empty consecutive subset with sum zero exists,
or NO otherwise.

## Sample input

```
2
5 -4 -7 10 6 4
5 -5 -2 -3 5 8
```

## Sample output

```
NO
YES
```

## Hint(s)

## Recommendation

We have carefully selected several similar problems:
[1000](http://coj.uci.cu/24h/problem.xhtml?pid=1000) |
[3378](http://coj.uci.cu/24h/problem.xhtml?pid=3378) |
[1049](http://coj.uci.cu/24h/problem.xhtml?pid=1049) |
[1494](http://coj.uci.cu/24h/problem.xhtml?pid=1494) |
[1028](http://coj.uci.cu/24h/problem.xhtml?pid=1028) |
[3599](http://coj.uci.cu/24h/problem.xhtml?pid=3599)

## Source

[Sum Challenge](http://coj.uci.cu/24h/problem.xhtml?pid=3147)
