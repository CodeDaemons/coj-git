# 3599 - Complementary Words

## Description

This time John has been playing with strings,
he has discovered the palindromes strings,
a palindrome string is one string that is read in the same way,
from left to right and from right to left.
John has made a small change to the definition
and called the result complementary words.

A word **S** is complementary if
for any pair
(0 <= i, j < **|S|** and i + j = **|S|**-1)
the sum of the character values
**S**[i] + **S**[j] is the same,
and values for a, b, ..., z are 1, 2, ..., 26.

## Input specification

The first line of input contains a non-empty string
**S** (1 <= **|S|** <= 3000)
consisting of lowercase letters.
The second line is an integer (1 <= N <= 10^6)
representing the number of queries to be performed,
line 3, 4, ..., N + 2 contains two integers A, B (0 <= A,B <= **|S|**-1).

## Output specification

For each query you must print "yes" if the continuous sub-sequence
S[A ... B] is a complementary word and "no" otherwise.

## Sample input

```
acecc
4
0 2
1 2
1 3
3 4
```

## Sample output

```
yes
yes
no
yes
```

## Hint(s)

## Recommendation

We have carefully selected several similar problems:
[1000](http://coj.uci.cu/24h/problem.xhtml?pid=1000) |
[3378](http://coj.uci.cu/24h/problem.xhtml?pid=3378) |
[1049](http://coj.uci.cu/24h/problem.xhtml?pid=1049) |
[1494](http://coj.uci.cu/24h/problem.xhtml?pid=1494) |
[1028](http://coj.uci.cu/24h/problem.xhtml?pid=1028) |
[1960](http://coj.uci.cu/24h/problem.xhtml?pid=1960)

## Source

[Complementary Words](http://coj.uci.cu/24h/problem.xhtml?pid=3599)
