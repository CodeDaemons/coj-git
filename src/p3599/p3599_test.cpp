/* Copyright (c) 2018 Kevin Villalobos */
#include "p3599.h"

#include <string>
#include <utility>
#include <vector>

#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

namespace utf = boost::unit_test;

BOOST_AUTO_TEST_CASE(acecc_0_2,
    *utf::timeout(2)) {
    const complementary_words::complementary_words word("acecc");

    std::pair<unsigned int, unsigned int> query(0, 2);
    const bool result = word.is_complementary(query);

    BOOST_REQUIRE(result);
}

BOOST_AUTO_TEST_CASE(acecc_1_2,
    *utf::timeout(2)) {
    const complementary_words::complementary_words word("acecc");

    std::pair<unsigned int, unsigned int> query(1, 2);
    const bool result = word.is_complementary(query);

    BOOST_REQUIRE(result);
}

BOOST_AUTO_TEST_CASE(acecc_1_3,
    *utf::timeout(2)) {
    const complementary_words::complementary_words word("acecc");

    std::pair<unsigned int, unsigned int> query(1, 3);
    const bool result = word.is_complementary(query);

    BOOST_REQUIRE(!result);
}

BOOST_AUTO_TEST_CASE(acecc_3_4,
    *utf::timeout(2)) {
    const complementary_words::complementary_words word("acecc");

    std::pair<unsigned int, unsigned int> query(3, 4);
    const bool result = word.is_complementary(query);

    BOOST_REQUIRE(result);
}

BOOST_AUTO_TEST_CASE(acecc_coj,
    *utf::timeout(2)) {
    const complementary_words::complementary_words word("acecc");

    std::pair<unsigned int, unsigned int> query1(0, 2);
    const bool result1 = word.is_complementary(query1);

    BOOST_REQUIRE(result1);

    std::pair<unsigned int, unsigned int> query2(1, 2);
    const bool result2 = word.is_complementary(query2);

    BOOST_REQUIRE(result2);

    std::pair<unsigned int, unsigned int> query3(1, 3);
    const bool result3 = word.is_complementary(query3);

    BOOST_REQUIRE(!result3);

    std::pair<unsigned int, unsigned int> query4(3, 4);
    const bool result4 = word.is_complementary(query4);

    BOOST_REQUIRE(result4);
}

BOOST_AUTO_TEST_CASE(long_string_true,
    *utf::timeout(2)) {
    std::string long_string = "";
    for (int i = 0; i < 3000; i++) {
        long_string += 'z';
    }

    const complementary_words::complementary_words word(long_string);

    for (int i = 0; i < 10e5; i++) {
        std::pair<unsigned int, unsigned int> query(0, 2999);
        const bool result = word.is_complementary(query);

        BOOST_REQUIRE(result);
    }
}

BOOST_AUTO_TEST_CASE(long_string_false,
    *utf::timeout(2)) {
    std::string long_string = "a";
    for (int i = 1; i < 3000; i++) {
        long_string += 'z';
    }

    const complementary_words::complementary_words word(long_string);

    for (int i = 0; i < 10e5 - 1; i++) {
        std::pair<unsigned int, unsigned int> query(0, 2999);
        const bool result_false = word.is_complementary(query);

        BOOST_REQUIRE(!result_false);
    }

    std::pair<unsigned int, unsigned int> query(1, 2999);
    const bool result_true = word.is_complementary(query);

    BOOST_REQUIRE(result_true);
}
