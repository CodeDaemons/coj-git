/* Copyright (c) 2018 Kevin Villalobos */
#include <iostream>
#include <string>
#include <utility>
#include "p3599.h"

namespace tools = complementary_words;

int main() {
    std::string S;
    std::cin >> S;
    tools::complementary_words word(S);

    unsigned int N;
    std::cin >> N;

    for (int i = 0; i < N; i++) {
        static std::pair<unsigned int, unsigned int> query;
        std::cin >> query.first >> query.second;

        switch (word.is_complementary(query)) {
            case false:
                std::cout << "no\n";
                break;
            case true:
                std::cout << "yes\n";
                break;
        }
    }

    return 0;
}
