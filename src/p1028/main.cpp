/* Copyright (c) 2018 Kevin Villalobos */
#include <iostream>
#include <string>
#include "p1028.h"

namespace tools = all_in_all;

int main() {
    std::string s, t;

    while (std::cin >> s >> t) {
        switch (tools::is_subsequence(s, t)) {
            case true:
                std::cout << "Yes" << "\n";
                break;
            case false:
                std::cout << "No" <<"\n";
                break;
        }
    }

    return 0;
}
