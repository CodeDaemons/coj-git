/* Copyright (c) 2018 Kevin Villalobos */
#include "p1028.h"

#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

namespace utf = boost::unit_test;
namespace tools = all_in_all;

BOOST_AUTO_TEST_CASE(test_true,
    *utf::timeout(2)) {
    const bool result = tools::is_subsequence(
        "sequence", "subsequence");

    BOOST_REQUIRE(result);
}

BOOST_AUTO_TEST_CASE(test_false,
    *utf::timeout(2)) {
    const bool result = tools::is_subsequence(
        "person", "compression");

    BOOST_REQUIRE(!result);
}

BOOST_AUTO_TEST_CASE(test_uppercase,
    *utf::timeout(2)) {
    const bool result = tools::is_subsequence(
        "VERDI", "vivaVittorioEmanueleReDiItalia");

    BOOST_REQUIRE(result);
}

BOOST_AUTO_TEST_CASE(test_almost,
    *utf::timeout(2)) {
    const bool result = tools::is_subsequence(
        "caseDoesMatter", "CaseDoesMatter");

    BOOST_REQUIRE(!result);
}

BOOST_AUTO_TEST_CASE(test_longest_true,
    *utf::timeout(2)) {
    const std::string string("abcdefghijklmnopqrstuvwxyzABCD");
    const std::string query("abcdefghijklmnopqrstuvwxyzABCD");

    const bool result = tools::is_subsequence(
        query, string);

    BOOST_REQUIRE(result);
}

BOOST_AUTO_TEST_CASE(test_longest_false,
    *utf::timeout(2)) {
    const std::string string("abcdefghijklmnopqrstuvwxyzABCD");
    const std::string query("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");

    const bool result = tools::is_subsequence(
        query, string);

    BOOST_REQUIRE(!result);
}

BOOST_AUTO_TEST_CASE(test_max_true,
    *utf::timeout(2)) {
    const std::string string("abcdefghijklmnopqrstuvwxyzABCD");
    const std::string query("abcdefghijklmnopqrstuvwxyzABCD");

    for (int i = 0; i < 20; i++) {
        const bool result = tools::is_subsequence(
            query, string);

        BOOST_REQUIRE(result);
    }
}

BOOST_AUTO_TEST_CASE(test_max_false,
    *utf::timeout(2)) {
    const std::string string("abcdefghijklmnopqrstuvwxyzABCD");
    const std::string query("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");

    for (int i = 0; i < 20; i++) {
        const bool result = tools::is_subsequence(
            query, string);

        BOOST_REQUIRE(!result);
    }
}
