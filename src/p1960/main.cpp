/* Copyright (c) 2018 Kevin Villalobos */
#include <iostream>
#include <tuple>
#include <vector>
#include "p1960.h"

namespace tools = connect_the_cows;

int main() {
    std::vector< std::tuple<int, int> > cows_vec;

    int n;
    std::cin >> n;

    int row, col;
    for (int i = 0; i < n; i++) {
        std::cin >> row >> col;
        cows_vec.push_back(std::tuple(row, col));
    }

    std::cout << tools::routes(cows_vec);

    return 0;
}
