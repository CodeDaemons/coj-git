/* Copyright (c) 2018 Kevin Villalobos */
#include "p1960.h"

#include <tuple>
#include <vector>

#define BOOST_TEST_MAIN
#include <boost/test/unit_test.hpp>

namespace utf = boost::unit_test;
namespace tools = connect_the_cows;

BOOST_AUTO_TEST_CASE(basic,
    *utf::timeout(2)) {
    const std::vector< std::tuple<int, int> > test_vec = {
        {0, 1}, {2, 1}, {2, 0}, {2, 5}
    };

    const int result = tools::routes(test_vec);

    BOOST_REQUIRE_EQUAL(2, result);
}
